+++
title = "About Metropolitanus"
date = "2014-04-09"
aliases = ["about","about-hugo","contact"]
[ author ]
  name = "Vanderbilt Hagans"

+++



The word [Metropolitan](https://en.wikipedia.org/wiki/Metropolitan) calls to mind many signs and symbols in the western, cultural imagination. The various uses and meanings of the word point to the economy, culture, colonialism and urban-ism.

The Metro-pole and Urban Culture
-------------------------------

Urban spaces in Western Europe and urban spaces in the United
States are usually culturally diverse. Codified by the cultural and
economic dichotomy between the [colony](https://en.wikipedia.org/wiki/Colony) and [metropole](https://en.wikipedia.org/wiki/Metropole), 21st century cities represents the confluence of the colony and metropole in postmodern, privatised, ritualised spaces.

20th century scholar and philosopher [Frantz Fanon](https://plato.stanford.edu/entries/frantz-fanon/) , an active participant in decolonisation "unmasked" the " hegemonic social and economic relationships formed between colony and metropole through the lens of social hierarchy which 'parcels out the world' by virtue of a politics of space founded on race (Ziarek 2002)." In other words, for Fanon, those unequal cultural, racial, economic and spacial relationships construct a transcendental norm for urban spaces over time. This unquestioned, unseen common sense perception of people, functions as a basic truth that upholds hegemony of the metropole over the colony that is granted and uncontroversial.

Fannon writes...

> the stark differences---politically, economically, and
> sociologically---between the colonized and the European sectors. The
> colonist's sector is a sector built to last, all stone and steel. It's
> a sector of lights and paved roads \[...\] the streets are clean and
> smooth, without a pothole, without a stone. The colonist's sector is
> sated, \[...\] its belly is permanently full of good things. 
>
> via: [White Skins Black Masks](https://en.wikipedia.org/wiki/Black_Skin,_White_Masks)

Fanon's Dialectic of Experience, analyses how the colonised suffer
violence in fixed, segregated space, or as Fanon might put it, "Manichean" regions of (non)being and mere subsistence. Additionally, For Fanon the metropole, rife with technology, infrastructure, culture and urban life represents a dichotomy between spaces and a hegemony of consumption.

![](https://static.cambridge.org/binary/version/id/urn:cambridge.org:id:binary:20160804153403344-0560:76162map16_1.png?pub-status=live)

Culturally, the embodiment of blackness or otherness among the colonised precedes and frames social relationships within the colonial transcendental norm where  ones essential being is fixed in time and space, like the black body in the west. Culture impacts spatiality to a point where the spaces people live transform politics and social relationships "into an extraordinary state of coercion." Those fixed traits solidify colonial norms as a part of the the [poststructural spacing and the fixed difference of colonised space](https://www.youtube.com/watch?v=6a2dLVx8THA&list=PLlUTWSCvO7mD8ysUKOoR12GtEG9Zp9Tf6&index=68&t=0s).



## Diffusion

Science Fiction writer H G Wells predicted the "[Probable Diffusion of
Great Cities](http://www.telelib.com/authors/W/WellsHerbertGeorge/prose/anticipations/anticipations002.html)in his work [Anticipations](http://www.telelib.com/authors/W/WellsHerbertGeorge/prose/anticipations/index.html). He predicted a future when

> The city will diffuse itself until it has taken up considerable area and many of the characteristics ... of what is now country. The old antithesis will indeed cease, the boundary lines will altogether disappear. 

Eschewing the colonial/metropolitan relationship Wells and [other British intelectuals](https://journals.openedition.org/ejas/11536) predicted the "posts", a post-colonial, post-structural spacing where the cultural and economic lines between colony and metropole become blurred through 21st century urban expansion. This invites thinkers to rethink the orientation of the colonial subject and metropolitan transcendence in the light of past hegemony associated with the western colonial project.

**[The City is Becoming World](http://urbantheorylab.net/site/assets/files/1076/madden_city_becoming_world.pdf)**

The notion that the world is becoming increasingly global and urban is a
meta-construct that informs this digital space. The end of the 20th
century was marked by a wave of western decolonisation. The decolonising
enterprise has a long way to go and in some respects has barely begun.
The urbanisation of society, however, includes economic and cultural
dilemmas that at once overlook colonial/metropole relationships and
simultaneously attempt to redefine similar hegemonic relationships
within the 21st century expansive city.

Madden (2012) points out.. 

> The urbanization of society is more than just a spatial process---it also involves forms of knowledge and frames for action.

Madden (2012) captures these 21st century actions in the concept of
"globalisation". On Globalisation he writes... 

> Many narratives of globalization---such as Thomas Friedman's (2005) well-known vision of a world characterized, among other things, by mass rural-to-urban migration and global competition---use the spectre of an interconnected planet in order to make neoliberal policies appear inevitable. Proceeding from the image of an omnicompetitive, interlinked urban world, management consultants and business analysts compile intricate quantitative hierarchies of global cities... that have the effect of steering politicians and planners further towards neoliberal urban policies.

### 3rd space

This digital space is focused on culture in its emergence from 3rd
Space or the emergence of post-colonial discourse and its critiques of cultural imperialism. Thirdspace is the heterogeneous synthesis of colony and metropole, which enact the process of cultural hybridity which gives rise to something different, something new in the 21st century

Thirdspace as Soja explains in Borch (2002) is...

> an-Other way of understanding and acting to change the spatiality of human life, a distinct mode of critical spatial awareness that is appropriate to the new scope and significance being brought about in the re-balanced trialectics of spatiality--historicality--sociality.\"...Thirdspace is a radically inclusive concept that encompasses epistemology, ontology, and historicity in continuous movement beyond dualisms and toward "an-Other": as Soja explains, "thirding produces what might best be called a cumulative trialectics that is radically open to additional otherness, to a continuing expansion of spatial knowledge."
>
> Thirdspace is a transcendent concept that is constantly expanding to
> include "an-Other," thus enabling the contestation and re-negotiation
> of boundaries and cultural identity.

![](https://lh3.googleusercontent.com/proxy/tA1EPqOG4Epw6jh81zy8HK7yIByyqb_MfqTETeHjM1YfyO4xecVnTu7jqwxWvbMeoD_PRe67xaZpENbShNGMujMZ0ZykMipL3d7AbvFJ-8bt9igMKMJKsHC2)

The heterogeneous synthesis of colony and metropole enact the process of
cultural hybridity which gives rise to something different, something new in
the 21st century. Metropolitanus is an account of the negotiation of
meaning and representation that goes along with 21st century life in the city and suburbs.



## Categories of interest

| Topic          | Consepts                                            |
| -------------- | :-------------------------------------------------- |
| Culture        | The signs and symbols of the urban west             |
| Hybridity      | Otherness defined away from the transcendental norm |
| Technology     | Digital and analogue tools                          |
| Transportation | Urban  transportation                               |











References
----------

Borch, Christian. 2002. "Interview with Edward W. Soja: Thirdspace,
Postmetropolis, and Social Theory." *Distinktion: Scandinavian Journal
of Social Theory* 3 (1): 113--20.
<https://doi.org/10.1080/1600910X.2002.9672816>.

Madden, David J. 2012. "City Becoming World: Nancy, Lefebvre, and the
Global Urban Imagination." *Environment and Planning D: Society and
Space* 30 (5): 772--87. <https://doi.org/10.1068/d17310>.

Ziarek, Ewa Płonowska. 2002. "Introduction: Fanon's Counterculture of
Modernity." *Parallax* 8 (2): 1--9.
<https://doi.org/10.1080/13534640210130386>.









## About the Open Source software that built this digital space

[Hugo](https://gohugo.io/) is the **world’s fastest framework for building websites**. It is written in Go.

It makes use of a variety of open source projects including:

* https://github.com/russross/blackfriday
* https://github.com/alecthomas/chroma
* https://github.com/muesli/smartcrop
* https://github.com/spf13/cobra
* https://github.com/spf13/viper

Learn more and contribute on [GitHub](https://github.com/gohugoio).
