---
title: "Zombies"
date: 2020-03-13T16:44:44Z
draft: false
toc: true
images:
categories: 
  - hybridity
  - culture
  - technology
tags:
  - embodiment
  - transcendental norm
  - zombie
  - podcast
---

# Zombie Podcasts: The Colony and the Metropole, History Hybridity and Culture

[On the "About" page](https://locdog.gitlab.io/Metropolitanus/about/) I mentioned Fanon's Dialectic of Experience, it analyses how the colonised suffer violence in fixed, segregated space, or as Fanon might put it, "Manichean" regions of (non)being and mere subsistence. Through a hybrid lens colonialism and it's various colonial possessions (including the colonised) are "Manichean" regions of (non)being and property are mere subsistence. The embodiment of the colonised is captured in the creature known as the black zombie. The black zombie represents a Manichean distortion of the Haitian (french colonial) image from the international community (western metropoles).  There are clear distinctions between black zombies and white zombies. These differences are layered with 20th century tropes on race, class, property, labour, capitalism and consumption.

## The black zombie

On the [New Books Network](https://newbooksnetwork.com/) of podcasts, [The African-American Studies podcast](https://newbooksnetwork.com/category/african-american-studies/)  featured  [Toni Pressley-Sanon](https://www.emich.edu/aaas/faculty/t-pressley-sanon.php) . She suggests zombies were born at the intersections of memory, history, and cultural production in both Africa and the African diaspora. Zombies provide an apt metaphor for Fanon's meaning of Manichean. The black zombie entered the western imagination through [William Seabrook’s book The Magic Island (1929)](https://www.goodreads.com/book/show/605545.The_Magic_Island), during the American occupation of Haiti. 

Pressley-Sanon points out that the origins of the Haitian zombie in the minds of US occupying forces (during Jim Crow era) was cultivated in "the desire to domesticate the black male at home while invading Caribbean countries." In this context Black bodies were no longer enslaved but engaged in  a process of subjugation through othering.  She states,

> Monsters must be contained or controlled.  The zombie is a being in need of contain and control. Zombies as metaphors for the uncontainable or uncontrollable need to be subjugated by controlling its labour to a being without will or its own mind.  Contain and control as a means of subjugating people who have been subjugated and should seek revenge.



![](https://images-na.ssl-images-amazon.com/images/I/61zBpXPjKrL._SX331_BO1,204,203,200_.jpg)

The black zombie represents the black body and the violence imposed on black bodies in the colony for the enrichment of the metropole. Representations of the black zombie range from  from enslaved worker to liberated rebel in revolutionary Haiti. 

**more on the Haitian revolution below**

Pressley-Sanon explains how she explores zombification as a consumptive process driven by capitalism in her book,  [Zombifying a Nation: Race, Gender and the Haitian Loas on Screen](https://networks.h-net.org/node/2881/reviews/536316/lee-pressley-sanon-zombifying-nation-race-gender-and-haitian-loas) 

**[Listen to the podcast here:](https://newbooksnetwork.com/toni-pressley-sanon-zombifying-a-nation-race-gender-and-the-haitian-loas-on-screen-mcfarland-2016/)**

## The white zombie 

[Sarah Juliet Lauro](https://www.ut.edu/directory/lauro-sarah-juliet) discusses the origins of the zombie, from enslaved worker to liberated rebel in revolutionary Haiti, to the consumer zombie of the 1960s, and today’s anxiety about societal collapse on [KPFA radio.](https://kpfa.org/episode/against-the-grain-april-23-2018/) Lauro is also the editor of [The Zombie Reader](https://www.upress.umn.edu/book-division/books/zombie-theory) an interdisciplinary collection of the best international scholarship on zombies as the embodiment of anxieties, critiques, and desires. 

**[Listen to the podcast here:](https://kpfa.org/episode/against-the-grain-april-23-2018/)**

![](https://www.upress.umn.edu/book-division/books/zombie-theory/image)



The [NPR podcast Throughline](https://www.npr.org/podcasts/510333/throughline) explores the movie [White Zombie](https://www.imdb.com/title/tt0023694/), that birthed the white zombie in the western imagination in 1932.

**[Listen to the podcast here:](https://www.npr.org/2019/10/30/774809210/zombies?t=1584107710264)**

![](https://media2.fdncms.com/chicago/imager/u/blog/19803303/zomb.jpg?cb=1454734860)



## Extras

### The Haitian Revolution Podcasts

Please also see Mike Duncan's** excellent podcast [Revolutions.](https://www.revolutionspodcast.com/)   This podcast provides a critical historical perspective of various revolutions of government over the 17th, 18th, 19th and 20th centuries. Each provides a study of social, economic and cultural relationships both between colonies and metropoles and within colonies and metropoles. 

The episodes highlighting the Haitian enslaved populations uprising and overthrow of the French are labelled The Haitian Revolution, however a listen to the episodes labelled the French Revolution will provide essential historical context. This series tracks Haitian colonial history from it's Spanish origins and French occupation to American occupation and Finally to Independence.

Haitian revolution
ep 1-3
**[Listen Here](https://www.revolutionspodcast.com/2015/12/index.html)**

![](https://thehistoryofrome.typepad.com/.a/6a01053629a711970c01bb08b0e290970d-800wi)

ep 4-8
**[Listen Here](https://www.revolutionspodcast.com/2016/01/index.html)**

![](https://thehistoryofrome.typepad.com/.a/6a01053629a711970c01bb08a697c2970d-800wi)

ep 9-12
**[Listen Here](https://www.revolutionspodcast.com/2016/02/index.html)**

![](https://thehistoryofrome.typepad.com/.a/6a01053629a711970c01b8d1b195eb970c-800wi)

ep 13-16
**[Listen Here](https://www.revolutionspodcast.com/2016/03/index.html)**

![](https://thehistoryofrome.typepad.com/.a/6a01053629a711970c01b7c83ab714970b-800wi)

ep 17-19
**[Listen Here](https://www.revolutionspodcast.com/2016/04/index.html)**

![](https://thehistoryofrome.typepad.com/.a/6a01053629a711970c01b8d1c4f352970c-800wi)



**Mike Duncan is also the creator of the amazing, [History of Rome podcast](https://thehistoryofrome.typepad.com/). Also in the Revolutions podcast series, the Colombian revolution provides another look at Spanish de-colonisation or the constitutional break between colony and metropole.







