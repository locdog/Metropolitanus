---
title: "Hybridity"
date: 2020-02-02T21:02:42Z
draft: false
toc: true
images:
categories: 
  - hybridity
tags:
  - embodiment
  - transcendental norm
---







## Embodiment

Understanding philosophy through what [Charles Mills](<https://www.gc.cuny.edu/faculty/core-bios/charles-w-mills>) describes as [non-cartisian sums](http://www.cornellpress.cornell.edu/book/?GCOI=80140100503600) or the way [we understand philosophy through the bodies](https://feministphilosophers.wordpress.com/2011/06/18/the-whiteness-of-philosophy-is-the-philosophy-a-problem/) in which we inhabit.  Second the black body in the west experiences historical and socio-cultural invisibility. This epistemic crisis is crystallised by [Ralph Ellison's](<http://www.read.gov/fiction/ellison.html>)  tome on [Invisibility](https://www.newyorker.com/books/page-turner/ralph-ellisons-invisible-man-as-a-parable-of-our-time)  or by the essential question, [ain't I a woman?](<https://www.nps.gov/articles/sojourner-truth.htm>) The interpretation of philosophy with the epistemological tool of [embodiment](<https://multimodalityglossary.wordpress.com/embodiment/>) is a hybrid post-colonial perspective.

 {{< youtube hvfs4jIYHrI >}}

[Invisible Man video book Summary by Chapter, Plot and Characters](https://www.youtube.com/playlist?list=PLz_ZtyOWL9BT3obwAgWy3uycLJew3Bl9n)

[The New Books Network spoke with John Callahan, Ellison’s literary executor and decades-long trusted friend overlooking Central Park in the waning light, looking at Harlem where  Ellison lived and where he’s buried today, and wondering whether, as the book’s final lines put it: “Who knows but that on the *lower frequencies*, I speak for you?”.](https://newbooksnetwork.com/great-books-john-callahan-on-ellisons-invisible-man/)



[Dr. George Yancy](http://georgeyancy.com/bio.html) explains embodiment through the transcendental norm of the white gaze at the black body below.

{{< youtube yaxhxsJ411I >}}



[So Much More on embodiment from Charles Mills, in his own words](<https://www.youtube.com/playlist?list=PLBZBrgpjOIknP7abYLjpUT6gfiTvDVhZq>)

## Blackness & Otherness in the West

In the modern era a thorough racialising rooted in colonialism  and later cotton industrial capital and blackness and otherness are defined on the backdrop of whiteness. In this dynamic white people are <u>seers and tellers of truth and history</u> and <u>black people have no histories and are seen</u>. 

## Whiteness as the transcendental norm  

[Within this transcendental norm](<https://plato.stanford.edu/entries/transcendental-arguments/>) blacks and other marginalised groups develop a different [axiological](<https://en.wikipedia.org/wiki/Axiology>) meaning. It is only against whiteness and maleness as transcendental norms that these meanings of "otherness" seem real or valid. In European philosophy (especially philosophy rooted in "enlightenment thought") there is the notion that whites are mind, spirit and represent freedom. Whereas blacks are reduced to their bodies. Blacks have experiences. The black is sensuous, the body dictates experience and it is base, raw, untamed and uncivilised. Therefore "the other" lack abstraction or the ability of extraction (these are the thoughts of [Hagel](<https://en.wikipedia.org/wiki/Hegelianism>).) Groups other than white are defined as deviant. Whiteness goes unnamed, ex-nominated, and invisible.  Whereas blackness gets named. This makes whiteness as a mark of privilege and power, a transcendental norm.

### 4 constructs a hybrid view of the world

1. Western societies are epistemically flawed in their understanding of marginalised groups
2. Western political society ie; Liberalism and Conservatism are racialised (and gendered)
3. Western society and Capitalism are racialised (and gendered)
4. Western society produces transcendental norms that create privilege, hegemony, and forms of spatial bonding as processes of western solidarity. 

## Hybrid Ontology

Hybridity is blackness and/or otherness defined away from the transcendental norm. It creates a new, self-defined, existential meaning. Hybridity creates space for the critique of the transcendental norm and the trans-formative power of deviations from the norm in order to develop a pragmatic, realist epistemic positioning.  [Double consciousness](<https://plato.stanford.edu/entries/double-consciousness/>), [The Harlem Renaissance](<https://www.history.com/topics/roaring-twenties/harlem-renaissance>), [Negritude](<https://www.blackpast.org/global-african-history/negritude-movement/>), are all 20th century iterations of hybridity focusing on the black body in the west.

## Hybridity

Through a Black/Other trans-Atlantic lens, hybridity in this digital space employs  perspectives and philosophy developed from the experiences and epistemological positions developed in Black/Other populations in the west during the modern era. This epistemic positioning is directly informed by the material reality of the African/Other diaspora(s) in the west and post-colonial states. It is anchored by a set of conceptual tools for a unique perspective in western philosophy, specifically the meaning of human being in the modern world.  The context for its exploration are colonialism and racism and motifs of enlightenment reason. 




