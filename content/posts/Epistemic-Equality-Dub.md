---
title: "Epistemic Equality Dub"
date: 2021-04-27T15:08:03+01:00
draft: false
toc: true
images:
categories: 
  - culture
tags:
  - ontology
  
  
---

# Audio

{{< imgfig "/images/EpistemicJusticeDubSL.jpg" >}}

{{< audio src="/audio/Epistemic_EqualityDub.ogg" >}}



# Text

## What is Epistemic Justice

According to [Miranda Fricker](https://www.mirandafricker.com/), ‘an epistemic injustice is a wrong done to someone specifically in their capacity as a knower or as an epistemic subject. 

> Epistemic justice (from the perspective of an agent who wishes to prevent wronging another) is ‘the proper inclusion and balancing of all epistemic sources’.  Epistemic resources  therefore are the interpretive resources or toolbox for understanding the testimony of the  unjust experiences others experience including their structurally prejudiced antecedents. Such tools help build relationships with people when they feel the social and interpretative resources in society do not address their needs. The moral valence of epistemic justice resides in the fact that epistemic practices are cooperative, and thus require reciprocity and fairness.  One may refer to those shared meanings, ideas and perceptions as the epistemic toolbox for living in society. (Fricker, 2013)


# References
MIRANDA FRICKER, (2013) *Epistemic Oppression and Epistemic Privilege*
[Published online: 01 Jul 2013.](http://core-cms.prod.aop.cambridge.org/core/journals/canadian-journal-of-philosophy-supplementary-volume/article/abs/epistemic-oppression-and-epistemic-privilege/A80AA050BD5FD15E8220279A722FBADD)

Miranda Fricker,(2013)  *Epistemic justice as a condition of political freedom?*
Synthese 190:1317–1332