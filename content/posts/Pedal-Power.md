---
title: "Pedal Power"
date: 2020-07-12T08:27:17+01:00
draft: false
toc: true
images:
categories: 
  - transportation
tags:
 - bicycle
---


## Flat Urban Pedals

As opposed to a status symbol bikes represent efficient transportation.  During the COVID-19 pandemic bicycles provide an efficient means of transportation that almost always maintains a 1 metre safe social distance. As an urban rider I do not wear any special cycling apparel, instead I wear everyday clothes. My old flat pedals would tear away at my shoes. The metal studs wrecked havoc on the soles of my shoes.

![](https://images.internetstores.de/products//537420/02/a0853d/537420_622a36_jpg[640x480].jpg?forceSize=true&forceAspectRatio=true&useTrim=true)







## Moto Reflex Pedals

I decided on flat pedals that would save shoes, I chose the German made  Moto Reflex pedals. The non-skid adhesive is very comfortable and shoe sole friendly.

{{< tweet 723538418899275779 >}}

The plastic base is not flimsy but I sometimes feel it flex when pumping hard. The pedals are an easy upgrade on urban bikes, see how they fit on my bike below.

## Installation


{{< youtube TNNN_N6jdng >}}


