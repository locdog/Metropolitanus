---
title: "Upright Bicycle Riding"
date: 2020-09-10T13:57:29+01:00
draft: false
toc: true
images:
categories: 
  - transportation
tags:
  - bicycle
---

## A Comfortable Ride
Upright cycling is a practical riding style in the city and suburbs. Sport-oriented road, mountain, hybrid and BMX bikes predominate the consumer market. Each have more aggressive cockpits and can strain the back. 

{{< tweet 857670852610891776 >}}

These types of bikes are geared towards performance and fitness and often feature more expensive components that also require more maintenance. 

## Everyday Urban Transportation

Upright bikes have a more comfortable cockpit and the ride is a casual  or slow roll from a to b. They are geared towards everyday riding and comfortable trips.


[Plain Bicycle](https://pod.link/1510268605)
[twitter](https://twitter.com/plainbicycle)


{{< tweet 1281966102847815686 >}}

{{< tweet 1279829073699758082 >}}


## Urban Practicality
Upright bikes are the two-wheeled standard for much of the world outside North America. While their names and styles vary, upright bikes often have additional luggage carrying features like racks, rear-wheel skirt guards, internal gear hubs and chain-guards, allowing riders to carry cargo, shift gears reliably and protect their clothes. 


{{< tweet 1279871756921012224 >}}

{{< tweet 1145602975748022278 >}}
 
 
## Upright Riding (A North American Primer)

{{< youtube -PpjOH2lR40 >}}
