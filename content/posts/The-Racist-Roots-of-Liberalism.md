---
title: "The Racist Roots of Liberalism"
date: 2021-04-23T15:47:58+01:00
draft: false
toc: true
images:
categories: 
  - culture
tags:
  - embodiment
  - transcendental norm
  - liberalism
---



## Audio

{{< imgfig "/images/mills7.jpeg" >}}

{{< audio src="/audio/The_Racist_Roots_of_Liberalism-LoFi.mp3" >}}



## Text

### Cartesianism

Cartesianism represents  the philosophical and scientific traditions derived from the writings of the French philosopher René Descartes (1596–1650). ["Descartes argues that one has certain knowledge of one’s own existence because one cannot think without knowing that one exists; this insight is expressed as “Cogito, ergo sum” (Latin: “I think, therefore I am”. The idea also was central to the  idealism of the German philosopher G.W.F. Hegel (1770–1831). Cartesians helped pioneer the use of dialetic to contrast two finite substances that represent human cognition, mind and matter](https://www.britannica.com/topic/Cartesianism). 



Mils (1994) articulates the problems of Cartesian philosophy as involving a whole program of assumptions about the world and (taken-for-granted) normative claims about what is philosophically important.   In short transcendentalism lies within it:

>  The enunciation of the Cartesian sum can be construed as one of the crucial episodes of European modernity. Here we have vividly portrayed the plight of the individual knower torn free from the sustaining verities of the dissolving feudal world, which had provided authority and certainty, and entering tentatively into the cognitive universe of an (as yet unrecognized) revolutionizing individualist capitalism, where all that is solid would melt into air. So the crucial question is posed: “what can I know?” And out of this, of course, comes modern epistemology, with the standard moves we all know, the challenges of skepticism, the danger of degeneration into solipsism, the idea of being enclosed in our own possibly unreliable perceptions, the question of whether we can know other minds exist, the scenario of brains-in-a-vat, etc. The Cartesian plight—represented as an allegedly universal predicament—and the foundationalist solution of knowledge of one’s own existence, thus becomes emblematic, a kind of pivotal scene for a whole way of doing philosophy, and involving a whole program of assumptions about the world and (taken-for-granted) normative claims about what is philosophically important. 



The genius of the Hegelian dialectic stands steadfast on the claim that any kind of opposition can simply be absorbed by this principle or be marginalised from the higher universal truth. Through the colonial/imperial gaze, the West are the makers of Universal History.

![](https://daughterofgodhome.files.wordpress.com/2019/01/images.png?w=640)

### Difference

> A lot of moral philosophy will then seem to be based on pretence, the claim that these were the principles that people strove to uphold, when in fact the real principles were the racially exclusivist ones. (Mills, 1994)

Placing a focus on "human" reason  is arguably a  powerful force in the development of human history and philosophy. As Mills (1994) states, reason "is rather conceived as a materially-conditioned principle operating within the realm of the human condition". The bio-political aspects of Cartesian cognition closely resemble the process of marginalisation within a political system. 

### The Racial Contract

Understanding difference through what Charles Mills describes as the way "we understand philosophy through the bodies in which we experience society, the Cartesian sum can be constructed as a crucial episode in European Enlightenment thinking" (Mills,2013). 

> The peculiar contract to which I am referring, though based on the social contract tradition that has been central to Western political theory, is not a contract between everybody ("we the people" but between just the people who count, the people who really are people ("we the white people"). (Mills, 1997)

Cartesian difference creates a Racial Contract.

> The "Racial Contract," then, is intended as a conceptual
> bridge between two areas now largely segregated from each other: on the one hand, the world of mainstream (i.e., white) ethics and political philosophy, preoccupied with discussions of justice and rights in the abstract, on the other hand, the world of Native American, African American, and Third and Fourth World political thought, historically focused on issues
> of conquest, imperialism, colonialism, white settlement, land rights, race and racism, slavery, jim crow, reparations, apart­heid, cultural authenticity, national identity, indigenismo, Af­rocentrism, etc... **The spatial, subcontract of the Racial Contract is the partitioning of the social world into ‘light’ and ‘dark’ spaces. This process happens literally through colonization and other crimes against people of color as well as through the semiotic by attaching differential meanings among social groups.** Through colonization, a spatial demarcation is enforced to expropriate lands and resources for the material advantages and cultural pleasures of European Whites.(Mills, 1997)

![](https://philosophy.utoronto.ca/wp-content/uploads/charles-mills-racial-contract-utoronto-philosophy.jpg)



## References

Mills, 1994, “Non-Cartesian Sums: Philosophy and the African-American Experience,”  occurs in Teaching Philosophy (vol 17, Issue 3, 1994).

Mills, C. W.,1997,The Racial Contract *Ithaca : Cornell University Press,* 

Mills, C. W., 2013, An Illuminating Blackness, *The Black Scholar,*, Vol. 43*,pp. 32-37











