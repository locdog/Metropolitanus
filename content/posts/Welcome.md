---
title: "Welcome"
date: 2020-01-31T17:27:56Z
draft: false
toc: true
images:
categories: 
  - culture
tags: 
  - welcome


---

**Metropolitanus** is an examination of the fabric of the city and suburbs, stitched together to form 21st century metropolitan areas. The word [Metropolitan](https://en.wikipedia.org/wiki/Metropolitan) brings to mind many signs and symbols in the cultural imagination. 

## Metropolitan Genres

Genres covered on this site include, but are not limited to:

[hybridity](https://www.oxfordreference.com/view/10.1093/oi/authority.20110803095952517)

culture

technology

transportation

















